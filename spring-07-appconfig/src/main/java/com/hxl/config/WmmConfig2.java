package com.hxl.config;

import com.hxl.pojo.User;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

//这个也会被Spring容器托管，注册到容器中，因为他本身就是一个@Component
//@Configuration代表这是一个Spring配置类，就和我们之前看的beans.xml是一样的
@Configuration
/*
也可以通过下面的方式进行扫描包
@ComponentScan("com.hxl.pojo")
*/

//将两个配置类搞成一个
@Import(WmmConfig.class)

public class WmmConfig2 {
    /*注册一个bean，就相当于我们之前写的一个bean标签。
    这个方法的名字，就相当于bean标签的id属性
    这个方法的返回值，就相当于bean标签的class属性
     */

    @Bean
    public User user(){
        return new User();//返回要注入到bean的对象
    }
}
