import com.hxl.pojo.Student;
import com.hxl.pojo.User;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MyTest {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
        Student student = (Student) context.getBean("student");
        System.out.println(student.toString());
        /*
        * Student{name='王木木', address=Address{address='null'}, books=[红楼梦, 水浒传], hobbys=[谈恋爱, 敲代码],
        * card={身份证=1254482515165451X, 银行卡=1549413214151515485}, games=[LOL, 王者荣耀], wife='null',
        *  info={学号=yj2021, 姓名=王木木}}

         * */
    }
    @Test
    public void test(){
        ApplicationContext context = new ClassPathXmlApplicationContext("userBeans.xml");
        User user = (User) context.getBean("user2");
        System.out.println(user.toString());


    }
}
