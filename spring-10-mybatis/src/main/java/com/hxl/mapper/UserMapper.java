package com.hxl.mapper;

import com.hxl.pojo.User;

import java.util.List;

public interface UserMapper {
    public List<User> selectUser();
}
