package com.hxl.service;

import com.hxl.dao.UserDao;
import com.hxl.dao.UserSqlDaoImpl;

public class UserServiceImpl implements UserService {
    private UserDao userDao;
    //利用set进行动态实现值的注入
    public void setUserDao(UserDao userDao){
        this.userDao = userDao;
    }
    public void getUser() {
        userDao.getUser();
    }
}
