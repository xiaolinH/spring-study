package com.hxl.pojo;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

// 等价于   <bean id="user" class="com.hxl.pojo.User"/>
//@Component 组件
@Component
@Scope("singleton")
public class User {
    //简单的可以用这个，复杂的还是用配置好
    //相当于<property name="name" value="王木木"/>
    //注解在set方法上也是可以的
    @Value("王木木")
    public String name;

    public void setName(String name) {
        this.name = name;
    }
}
